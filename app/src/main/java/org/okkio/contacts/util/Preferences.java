package org.okkio.contacts.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.okkio.contacts.model.Hello;

public class Preferences {
    private static final String TAG = "preferences";

    private static final String SHARED_PREF_NAME = "org.okkio.contacts.pref";
    private static final String SHARED_PREF_KEY_AUTH = "lp";

    public static Hello load(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String prefs = sharedPreferences.getString(SHARED_PREF_KEY_AUTH, null);
        if (prefs == null) {
            return null;
        }
        try {
            String data = AESEncryption.decrypt(prefs);
            String[] result = data.split("::");
            return new Hello(result[0], result[1]);
        } catch (Exception e) {
            Log.i(TAG, "Can/'t decrypt data. oOps.");
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(SHARED_PREF_KEY_AUTH).apply();
        }

        return null;
    }

    public static void save(Context context, Hello data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String string = data.getUsername() + "::" + data.getPassword();
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(SHARED_PREF_KEY_AUTH, AESEncryption.encrypt(string)).apply();
        } catch (Exception e) {
            Log.i(TAG, "Can/'t encrypt data. Don/'t saved.");
        }
    }

    public static void remove(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SHARED_PREF_KEY_AUTH).apply();
    }
}
