package org.okkio.contacts.util;

import android.util.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption {

    private static final int ITERATION = 10;
    private static final int KEY_SIZE = 128;
    private static final String CYPHER_INSTANCE = "AES/CBC/PKCS5Padding";
    private static final String SECRET_KEY_INSTANCE = "PBKDF2WithHmacSHA1";
    private static final String PLAIN_TEXT = "Megatron";
    private static final String AES_SALT = "Ironhide";
    private static final String INITIALIZATION_VECTOR = "8119745113154120";

    public static String encrypt(String textToEncrypt) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(getRaw(PLAIN_TEXT, AES_SALT), "AES");
        Cipher cipher = Cipher.getInstance(CYPHER_INSTANCE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(INITIALIZATION_VECTOR.getBytes()));
        byte[] encrypted = cipher.doFinal(textToEncrypt.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }

    public static String decrypt(String textToDecrypt) throws Exception {
        byte[] encryptedBytes = Base64.decode(textToDecrypt, Base64.DEFAULT);
        SecretKeySpec secretKeySpec = new SecretKeySpec(getRaw(PLAIN_TEXT, AES_SALT), "AES");
        Cipher cipher = Cipher.getInstance(CYPHER_INSTANCE);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(INITIALIZATION_VECTOR.getBytes()));
        byte[] decrypted = cipher.doFinal(encryptedBytes);
        return new String(decrypted, "UTF-8");
    }

    private static byte[] getRaw(String plainText, String salt) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_INSTANCE);
            KeySpec spec = new PBEKeySpec(plainText.toCharArray(), salt.getBytes(), ITERATION, KEY_SIZE);
            return factory.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
