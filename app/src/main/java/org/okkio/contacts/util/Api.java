package org.okkio.contacts.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.okkio.contacts.model.Department;
import org.okkio.contacts.model.Employee;
import org.okkio.contacts.model.Node;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Api {
    private static final String TAG = "API";

    private static final String BASE_URL = "https://contact.taxsee.com/Contacts.svc/";
    private static final String METHOD_HELLO = "Hello";
    private static final String METHOD_GET_DATA = "GetAll";
    private static final String METHOD_GET_PHOTO = "GetWPhoto";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";

    private String mLogin;
    private String mPassword;

    public static Api auth(String login, String password) {
        return new Api(login, password);
    }

    public Api(String login, String password) {
        mLogin = login;
        mPassword = password;
    }

    public boolean hello() {
        boolean success = false;
        try {
            String jsonString = new String(download(buildUrl(METHOD_HELLO)));
            JSONObject json = new JSONObject(jsonString);
            success = json.getBoolean("Success");
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch JSON", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return success;
    }

    public List<Node> getData() {
        try {
            String jsonString = new String(download(buildUrl(METHOD_GET_DATA)));
            JSONObject json = new JSONObject(jsonString);
            List<Node> nodeList = new ArrayList<>();
            nodeList.add(parseDepartment(json));
            return nodeList;
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch JSON", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }
        return null;
    }

    public Bitmap getPhoto(int id) {
        Map<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(id));
        String url = buildUrl(METHOD_GET_PHOTO, params);
        return getPhoto(url);
    }

    public static Bitmap getPhoto(String url) {
        Bitmap bitmap = null;
        try {
            byte[] bitmapBytes = download(url);
            bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed load bitmap", ioe);
        }
        return bitmap;
    }

    private static byte[] download(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    private Node parseDepartment(JSONObject object) throws JSONException {
        Node<Department> department = new Node<>(new Department(object.getInt("ID"), object.getString("Name")));

        if (object.has("Departments")) {
            JSONArray departments = object.getJSONArray("Departments");
            for (int i = 0; i < departments.length(); i++) {
                department.addChild(parseDepartment(departments.getJSONObject(i)));
            }
        }

        if (object.has("Employees")) {
            JSONArray employees = object.getJSONArray("Employees");
            for (int i = 0; i < employees.length(); i++) {
                department.addChild(parseEmployee(employees.getJSONObject(i)));
            }
        }
        return department;
    }

    private Node parseEmployee(JSONObject object) throws JSONException {
        int id = object.getInt("ID");
        String email = object.has("Email") ? object.getString("Email") : "";
        String phone = object.has("Phone") ? object.getString("Phone") : "";
        Map<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(id));
        String photoUrl = buildUrl(METHOD_GET_PHOTO, params);
        return new Node<>(new Employee(
                id,
                object.getString("Name"),
                object.getString("Title"),
                email,
                phone,
                photoUrl));
    }

    @NonNull
    private String buildUrl(String method) {
        return buildUrl(method, null);
    }

    @NonNull
    private String buildUrl(String method, Map<String, String> params) {
        Uri.Builder builder = Uri.parse(BASE_URL).buildUpon().appendPath(method);
        if (params != null) {
            for (Map.Entry entry : params.entrySet()) {
                builder.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
            }
        }
        builder.appendQueryParameter(PARAM_LOGIN, mLogin);
        builder.appendQueryParameter(PARAM_PASSWORD, mPassword);
        return builder.build().toString();
    }

}
