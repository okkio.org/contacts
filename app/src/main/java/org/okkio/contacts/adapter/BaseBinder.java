package org.okkio.contacts.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.okkio.contacts.model.Item;
import org.okkio.contacts.model.Node;

public abstract class BaseBinder<VH extends RecyclerView.ViewHolder> implements Item {
    public abstract VH provideViewHolder(View itemView);

    public abstract void bindView(VH holder, int position, Node<? extends Item> node);

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View parent) {
            super(parent);
        }
    }
}
