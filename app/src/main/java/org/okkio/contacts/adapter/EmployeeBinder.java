package org.okkio.contacts.adapter;

import android.view.View;
import android.widget.TextView;

import org.okkio.contacts.R;
import org.okkio.contacts.model.Employee;
import org.okkio.contacts.model.Node;

public class EmployeeBinder extends BaseBinder<EmployeeBinder.ViewHolder> {

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, Node node) {
        Employee fileNode = (Employee) node.getRow();
        holder.mNameView.setText(fileNode.getName());
        holder.mTitleView.setText(fileNode.getTitle());
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_employee;
    }

    public class ViewHolder extends BaseBinder.ViewHolder {
        public TextView mNameView, mTitleView;

        public ViewHolder(View rootView) {
            super(rootView);
            this.mNameView = rootView.findViewById(R.id.name);
            this.mTitleView = rootView.findViewById(R.id.title);
        }

    }
}
