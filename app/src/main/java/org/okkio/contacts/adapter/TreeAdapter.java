package org.okkio.contacts.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.okkio.contacts.model.Node;

import java.util.ArrayList;
import java.util.List;

public class TreeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String KEY = "IS_EXPAND";
    private List<? extends BaseBinder> mBinders;
    private List<Node> mVisibleNodes;
    private int padding = 30;
    private OnClickNodeListener mClickNodeListener;
    private boolean mIsCollapseChild = false;

    public TreeAdapter(List<Node> nodes, List<? extends BaseBinder> binders) {
        if (nodes != null)
            findVisibleNodes(nodes);
        mBinders = binders;
    }

    private void findVisibleNodes(List<Node> nodes) {
        mVisibleNodes = new ArrayList<Node>();
        for (Node node : nodes) {
            mVisibleNodes.add(node);
            if (!node.isLeaf() && node.isExpand()) {
                findVisibleNodes(node.getChildList());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mVisibleNodes.get(position).getRow().getLayoutId();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(viewType, viewGroup, false);
        if (mBinders.size() == 1) {
            return mBinders.get(0).provideViewHolder(v);
        }

        for (BaseBinder adapter : mBinders) {
            if (adapter.getLayoutId() == viewType) {
                return adapter.provideViewHolder(v);
            }
        }

        return mBinders.get(0).provideViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (!payloads.isEmpty()) {
            Bundle bundle = (Bundle) payloads.get(0);
            for (String key : bundle.keySet()) {
                if (key.equals(KEY)) {
                    if (mClickNodeListener != null) {
                        mClickNodeListener.onToggle(bundle.getBoolean(key), holder);
                    }
                    break;
                }
            }
        }
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
        viewHolder.itemView.setPadding(mVisibleNodes.get(i).getHeight() * padding, 3, 3, 3);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Node currentNode = mVisibleNodes.get(viewHolder.getLayoutPosition());
                // Prevent multi-click
                try {
                    long lastClick = (long) viewHolder.itemView.getTag();
                    if (System.currentTimeMillis() - lastClick < 500) {
                        return;
                    }
                } catch (Exception e) {
                    viewHolder.itemView.setTag(System.currentTimeMillis());
                }
                viewHolder.itemView.setTag(System.currentTimeMillis());

                if (mClickNodeListener != null && mClickNodeListener.onClick(currentNode, viewHolder)) {
                    return;
                }
                if (currentNode.isLeaf()) {
                    return;
                }
                boolean isExpand = currentNode.isExpand();
                int positionStart = mVisibleNodes.indexOf(currentNode) + 1;
                if (!isExpand) {
                    notifyItemRangeInserted(positionStart, addChildNode(currentNode, positionStart));
                } else {
                    notifyItemRangeRemoved(positionStart, removeChildNode(currentNode, true));
                }
            }
        });
        for (BaseBinder adapter : mBinders) {
            if (adapter.getLayoutId() == mVisibleNodes.get(i).getRow().getLayoutId()) {
                adapter.bindView(viewHolder, i, mVisibleNodes.get(i));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mVisibleNodes.size();
    }

    private int addChildNode(Node node, int startIndex) {
        List<Node> childList = node.getChildList();
        int countAdded = 0;
        for (Node child : childList) {
            mVisibleNodes.add(startIndex + countAdded++, child);
            if (child.isExpand()) {
                countAdded += addChildNode(child, startIndex + countAdded);
            }
        }
        if (!node.isExpand())
            node.toggle();
        return countAdded;
    }

    private int removeChildNode(Node node) {
        return removeChildNode(node, true);
    }

    private int removeChildNode(Node node, boolean shouldToggle) {
        if (node.isLeaf()) {
            return 0;
        }
        List<Node> childList = node.getChildList();
        int countRemoved = childList.size();
        mVisibleNodes.removeAll(childList);
        for (Node child : childList) {
            if (child.isExpand()) {
                if (mIsCollapseChild) {
                    child.toggle();
                }
                countRemoved += removeChildNode(child, false);
            }
        }
        if (shouldToggle) {
            node.toggle();
        }
        return countRemoved;
    }

    public void setClickNodeListener(OnClickNodeListener listener) {
        mClickNodeListener = listener;
    }

    public interface OnClickNodeListener {
        boolean onClick(Node node, RecyclerView.ViewHolder holder);

        void onToggle(boolean isExpand, RecyclerView.ViewHolder holder);
    }
}
