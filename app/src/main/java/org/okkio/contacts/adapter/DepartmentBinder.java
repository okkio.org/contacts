package org.okkio.contacts.adapter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.okkio.contacts.R;
import org.okkio.contacts.model.Department;
import org.okkio.contacts.model.Node;

public class DepartmentBinder extends BaseBinder<DepartmentBinder.ViewHolder> {
    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, Node node) {
        holder.mArrowView.setRotation(0);
        holder.mArrowView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_18dp);
        int rotateDegree = node.isExpand() ? 90 : 0;
        holder.mArrowView.setRotation(rotateDegree);
        Department dirNode = (Department) node.getRow();
        holder.mNameView.setText(dirNode.getName());
        if (node.isLeaf())
            holder.mArrowView.setVisibility(View.INVISIBLE);
        else holder.mArrowView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_department;
    }

    public static class ViewHolder extends BaseBinder.ViewHolder {
        private ImageView mArrowView;
        private TextView mNameView;

        public ViewHolder(@NonNull View parent) {
            super(parent);
            this.mArrowView = parent.findViewById(R.id.arrow);
            this.mNameView = parent.findViewById(R.id.name);
        }

        public ImageView getArrowView() {
            return mArrowView;
        }

        public TextView getNameView() {
            return mNameView;
        }
    }
}
