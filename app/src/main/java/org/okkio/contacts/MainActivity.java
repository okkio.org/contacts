package org.okkio.contacts;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.okkio.contacts.adapter.DepartmentBinder;
import org.okkio.contacts.adapter.EmployeeBinder;
import org.okkio.contacts.adapter.TreeAdapter;
import org.okkio.contacts.model.Employee;
import org.okkio.contacts.model.Hello;
import org.okkio.contacts.model.Node;
import org.okkio.contacts.util.Api;
import org.okkio.contacts.util.Preferences;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TreeAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private AppBarLayout mAppBarLayout;
    private Hello mHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        mRecyclerView = findViewById(R.id.recycler_view);
        View signOutView = findViewById(R.id.sign_out);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toggleElevationAppbar();
        }

        mHello = Preferences.load(this);

        // sign out
        signOutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.remove(MainActivity.this);
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        initData();
    }

    private void initData() {
        mProgressDialog.show();
        FetchDataTask task = new FetchDataTask();
        task.setListener(new FetchDataTask.AsyncTaskListener() {
            @Override
            public void onAsyncTaskFinished(List<Node> result) {
                mProgressDialog.dismiss();
                if (result == null) {
                    Toast.makeText(MainActivity.this, getString(R.string.fetch_error), Toast.LENGTH_SHORT).show();
                    return;
                }
                mAdapter = new TreeAdapter(result, Arrays.asList(new DepartmentBinder(), new EmployeeBinder()));
                mAdapter.setClickNodeListener(new TreeAdapter.OnClickNodeListener() {
                    @Override
                    public boolean onClick(Node node, RecyclerView.ViewHolder holder) {
                        if (node.isLeaf()) {
                            if (node.getRow() instanceof Employee) {
                                Dialog mDialog = Dialog.newInstance((Employee) node.getRow());
                                mDialog.show(getSupportFragmentManager(), "employee");
                                //Toast.makeText(MainActivity.this, employee.getId() + " " + employee.getName(), Toast.LENGTH_SHORT).show();
                            } /*else if (node.getRow() instanceof Department) {
                                Toast.makeText(MainActivity.this, "No childs", Toast.LENGTH_SHORT).show();
                            }*/
                        } else {
                            onToggle(!node.isExpand(), holder);
                        }

                        return false;
                    }

                    @Override
                    public void onToggle(boolean isExpand, RecyclerView.ViewHolder holder) {
                        DepartmentBinder.ViewHolder dirViewHolder = (DepartmentBinder.ViewHolder) holder;
                        final ImageView ivArrow = dirViewHolder.getArrowView();
                        int rotateDegree = isExpand ? 90 : -90;
                        ivArrow.animate().rotationBy(rotateDegree).start();
                    }
                });
                mRecyclerView.setAdapter(mAdapter);
            }
        });

        task.execute(mHello.getUsername(), mHello.getPassword());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void toggleElevationAppbar() {
        mAppBarLayout = findViewById(R.id.app_bar_layout);
        mAppBarLayout.setElevation(0);
        // get initial position
        final int initialTopPosition = mRecyclerView.getTop();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                ValueAnimator animator = null;
                float elevation = mAppBarLayout.getElevation();
                if (mRecyclerView.getChildAt(0).getTop() < initialTopPosition) {
                    if (elevation < 10) {
                        animator = ValueAnimator.ofFloat(elevation, 10);
                    }
                } else {
                    if (mAppBarLayout.getElevation() != 0) {
                        animator = ValueAnimator.ofFloat(elevation, 0);
                    }
                }
                if (animator != null) {
                    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            mAppBarLayout.setElevation((Float) animation.getAnimatedValue());
                        }
                    });
                    animator.start();
                }
            }
        });
    }

    private static class FetchDataTask extends AsyncTask<Object, Void, List<Node>> {
        private AsyncTaskListener listener;

        @Override
        protected List<Node> doInBackground(Object... params) {
            return Api.auth((String) params[0], (String) params[1]).getData();
        }

        @Override
        protected void onPostExecute(List<Node> result) {
            super.onPostExecute(result);
            listener.onAsyncTaskFinished(result);
        }

        public void setListener(AsyncTaskListener listener) {
            this.listener = listener;
        }


        public interface AsyncTaskListener {
            void onAsyncTaskFinished(List<Node> result);
        }
    }
}
