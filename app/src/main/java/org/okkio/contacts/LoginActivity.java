package org.okkio.contacts;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.okkio.contacts.model.Hello;
import org.okkio.contacts.util.Api;
import org.okkio.contacts.util.Preferences;

public class LoginActivity extends AppCompatActivity {
    private FetchHelloTask task;
    private Button mButtonView;
    private EditText mUsernameView, mPasswordView;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mButtonView = findViewById(R.id.button);
        mUsernameView = findViewById(R.id.username);
        mPasswordView = findViewById(R.id.password);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        // autologin
        Hello hello = Preferences.load(this);
        if (hello != null) {
            mUsernameView.setText(hello.getUsername());
            hello(hello.getUsername(), hello.getPassword());
        }

        // for Done button on keyboard
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hello(mUsernameView.getText().toString(), mPasswordView.getText().toString());
                    return true;
                }
                return false;
            }
        });

        mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hello(mUsernameView.getText().toString(), mPasswordView.getText().toString());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void hello(final String username, final String password) {
        if (username.length() == 0 || password.length() == 0) {
            return;
        }
        mProgressDialog.show();
        FetchHelloTask task = new FetchHelloTask();
        task.setListener(new FetchHelloTask.AsyncTaskListener() {
            @Override
            public void onAsyncTaskFinished(boolean result) {
                mProgressDialog.dismiss();
                if (result) {
                    Preferences.save(LoginActivity.this, new Hello(username, password));
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(LoginActivity.this, getString(R.string.auth_error), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            }
        });
        //task.execute("test_user", "test_pass");
        task.execute(username, password);
    }

    private static class FetchHelloTask extends AsyncTask<Object, Void, Boolean> {
        private AsyncTaskListener listener;

        @Override
        protected Boolean doInBackground(Object... params) {
            return Api.auth((String) params[0], (String) params[1]).hello();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            listener.onAsyncTaskFinished(result);
        }

        public void setListener(AsyncTaskListener listener) {
            this.listener = listener;
        }

        public interface AsyncTaskListener {
            void onAsyncTaskFinished(boolean result);
        }
    }
}
