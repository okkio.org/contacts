package org.okkio.contacts.model;

import org.okkio.contacts.R;

import java.util.ArrayList;
import java.util.List;

public class Department implements Item {
    private int mId;
    private String mName;
    private List<Employee> mEmployees = new ArrayList<>();
    private List<Department> mDepartments = new ArrayList<>();

    public Department(int id, String name) {
        mId = id;
        mName = name;
    }

    public Department(String name) {
        mName = name;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_department;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Employee> getEmployees() {
        return mEmployees;
    }

    public void setEmployees(List<Employee> employees) {
        mEmployees = employees;
    }

    public void addEmployee(Employee employee) {
        mEmployees.add(employee);
    }

    public List<Department> getDepartments() {
        return mDepartments;
    }

    public void setDepartments(List<Department> departments) {
        mDepartments = departments;
    }

    public void addDepartment(Department department) {
        mDepartments.add(department);
    }
}
