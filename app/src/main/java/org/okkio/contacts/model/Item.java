package org.okkio.contacts.model;

public interface Item {
    int getLayoutId();
}
