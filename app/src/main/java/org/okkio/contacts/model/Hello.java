package org.okkio.contacts.model;

public class Hello {
    private String username;
    private String password;

    public Hello(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
