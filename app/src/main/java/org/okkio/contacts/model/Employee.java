package org.okkio.contacts.model;

import org.okkio.contacts.R;

public class Employee implements Item {
    private int mId;
    private String mName;
    private String mTitle;
    private String mEmail;
    private String mPhone;
    private String mPhotoUrl;

    public Employee(int id, String name, String title, String email, String phone, String photoUrl) {
        mId = id;
        mName = name;
        mTitle = title;
        mEmail = email;
        mPhone = phone;
        mPhotoUrl = photoUrl;
    }

    public Employee(String name) {
        mName = name;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_employee;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }
}
