package org.okkio.contacts.model;

import java.util.ArrayList;
import java.util.List;

public class Node<T extends Item> {
    private T mRow;
    private Node mParent;
    private List<Node<? extends Item>> mChildList = new ArrayList<>();
    private boolean mIsExpand;
    private int mHeight = -1;

    public Node(T row) {
        mRow = row;
    }

    public boolean isRoot() {
        return mParent == null;
    }

    public boolean isLeaf() {
        return mChildList == null || mChildList.isEmpty();
    }

    public boolean isExpand() {
        return mIsExpand;
    }

    public T getRow() {
        return mRow;
    }

    public List<Node<? extends Item>> getChildList() {
        return mChildList;
    }

    public int getHeight() {
        if (isRoot()) {
            mHeight = 0;
        }
        return mHeight == -1 ? mParent.getHeight() + 1 : mHeight;
    }

    public Node addChild(Node<? extends Item> node) {
        mChildList.add(node);
        node.mParent = this;
        return this;
    }

    public boolean toggle() {
        mIsExpand = !mIsExpand;
        return mIsExpand;
    }

    public void expand() {
        mIsExpand = true;
    }

    public void collapse() {
        mIsExpand = false;
    }

}
