package org.okkio.contacts;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.okkio.contacts.model.Employee;
import org.okkio.contacts.util.Api;

public class Dialog extends DialogFragment {
    final String TAG = "Dialog";

    private String mEmail;
    private String mPhone;
    private String mPhotoUrl;
    private ImageView mPhoto;

    public static Dialog newInstance(Employee employee) {
        Dialog dialog = new Dialog();
        Bundle args = new Bundle();
        args.putString("name", employee.getName());
        args.putString("title", employee.getTitle());
        args.putString("email", employee.getEmail());
        args.putString("phone", employee.getPhone());
        args.putString("photoUrl", employee.getPhotoUrl());
        dialog.setArguments(args);

        return dialog;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Title!");
        View v = inflater.inflate(R.layout.dialog, container, false);
        final ImageView mPhoto = v.findViewById(R.id.photo);
        TextView mNameView = v.findViewById(R.id.name);
        TextView mTitleView = v.findViewById(R.id.title);
        final TextView mPhoneView = v.findViewById(R.id.phone_value);
        TextView mEmailView = v.findViewById(R.id.email_value);
        if (getArguments() != null) {
            mEmail = getArguments().getString("email");
            mPhone = getArguments().getString("phone");
            mPhotoUrl = getArguments().getString("photoUrl");
            mNameView.setText(getArguments().getString("name"));
            mTitleView.setText(getArguments().getString("title"));
            if (mEmail.length() > 0) {
                mEmailView.setText(mEmail);
                mEmailView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mEmail, null));
                        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
                    }
                });
            } else {
                v.findViewById(R.id.email).setVisibility(View.GONE);
            }
            if (mPhone.length() > 0) {
                mPhoneView.setText(mPhone);
                mPhoneView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mPhone, null));
                        startActivity(intent);
                    }
                });
            } else {
                v.findViewById(R.id.phone).setVisibility(View.GONE);
            }
            Log.d("qwerty", mPhotoUrl);
            FetchBitmapTask task = new FetchBitmapTask();
            task.setListener(new FetchBitmapTask.AsyncTaskListener() {
                @Override
                public void onAsyncTaskFinished(Bitmap result) {
                    if(result != null) {
                        mPhoto.setImageBitmap(result);
                    }
                }
            });

            task.execute(mPhotoUrl);
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog().getWindow() == null) {
            return;
        }
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    private static class FetchBitmapTask extends AsyncTask<Object, Void, Bitmap> {
        private AsyncTaskListener listener;

        @Override
        protected Bitmap doInBackground(Object... params) {
            return Api.getPhoto((String) params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            listener.onAsyncTaskFinished(result);
        }

        public void setListener(AsyncTaskListener listener) {
            this.listener = listener;
        }

        public interface AsyncTaskListener {
            void onAsyncTaskFinished(Bitmap result);
        }
    }
}
